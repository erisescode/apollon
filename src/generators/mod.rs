mod ansi;
mod html;

pub use ansi::*;
pub use html::*;

use crate::processor::DocToken;

/// Enum to represent the built in generators in apollon
#[derive(Clone, clap::ArgEnum, Debug)]
pub enum InbuiltGenerators {
    /// Representation for `AnsiGenerator`
    ANSI,
    /// Representation for `HtmlGenerator`
    HTML,
}

/// Trait implemented by generators,
/// Used for dynamic generation.
pub trait Generator {
    /// Formats a single token.
    fn format_token(&mut self, token: &DocToken) -> String;
    /// Generates a formatted result of a parsed document.
    fn generate(&mut self, tokens: &Vec<DocToken>) -> String {
        tokens.into_iter().map(|tk| self.format_token(tk)).collect()
    }
}
