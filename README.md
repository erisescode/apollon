# Apollon

*Apollon* is a simple markup (akin to roff)
which values simplicity over beauty or fanciness.

Apollon uses a system very similar to roff.
It doesn't support macro packages (yet),
and has more human readable tags (in my opinion)

For usage and install instructions check out the 
[User Manual](https://gitlab.com/erisescode/apollon/~/blob/master/userman.org).
